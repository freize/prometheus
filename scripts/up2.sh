#!/bin/bash
#---------------------------------------------------------------
#-Скрипт разработан специально для 4PDA от Foreman (Freize.org)-
#-Распространение без ведома автора запрещено!                 -
#---------------------------------------------------------------
#Цвета:
RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[1;36m'
YELLOW='\033[1;33m'
NONE='\033[0m'
#---------------------------------------------------------------
# Конец технического раздела
#---------------------------------------------------------------
echo -e "$YELLOW Устанавливаем ПО, требуется ввести пароль от $NONE"
echo -e "$YELLOW вашей учетной записи. $NONE"
sudo apt-get update
sudo apt-get -y --force-yes install build-essential gawk texinfo pkg-config gettext automake libtool bison flex zlib1g-dev libgmp3-dev libmpfr-dev libmpc-dev git lighttpd zip sshpass mc
#---------------------------------------------------------------
# Конец скрипта
#---------------------------------------------------------------
